// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyD-deIb_zKnR1iCy6l3dwZ43yig2Qk-V14',
    authDomain: 'fitness-tracker-70433.firebaseapp.com',
    databaseURL: 'https://fitness-tracker-70433.firebaseio.com',
    projectId: 'fitness-tracker-70433',
    storageBucket: 'fitness-tracker-70433.appspot.com',
    messagingSenderId: 1069840166060
  }
};
