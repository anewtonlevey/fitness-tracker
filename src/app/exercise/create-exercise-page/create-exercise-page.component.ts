import { Component, OnInit } from '@angular/core';
import { Exercise, ExerciseCls } from '../../shared/models/exercise.model';
import { EditExerciseState, AppState } from '../../state/app.state';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { getEditExerciseState } from '../../state/reducers/exercises.reducer';

@Component({
  selector: 'app-create-exercise-page',
  templateUrl: './create-exercise-page.component.html',
  styleUrls: ['./create-exercise-page.component.scss']
})
export class CreateExercisePageComponent implements OnInit {

  exercise: Exercise = new ExerciseCls();
  exerciseState$: Observable<EditExerciseState>;

  constructor(private store: Store<AppState>) { }

  ngOnInit() {
    this.exerciseState$ = this.store.select(getEditExerciseState);
  }

}
