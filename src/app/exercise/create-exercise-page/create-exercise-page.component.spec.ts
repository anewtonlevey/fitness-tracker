import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateExercisePageComponent } from './create-exercise-page.component';

describe('CreateExercisePageComponent', () => {
  let component: CreateExercisePageComponent;
  let fixture: ComponentFixture<CreateExercisePageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateExercisePageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateExercisePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
