import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Exercise } from '../../shared/models/exercise.model';
import { AppState, EditExerciseState } from '../../state/app.state';
import { Store } from '@ngrx/store';
import { getEditExerciseState, getExerciseState } from '../../state/reducers/exercises.reducer';
import { map } from 'rxjs/operators';
import { EditExerciseAction } from '../../state/actions/exercises.actions';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-edit-exercise-page',
  templateUrl: './edit-exercise-page.component.html',
  styleUrls: ['./edit-exercise-page.component.scss']
})
export class EditExercisePageComponent implements OnInit {

  exerciseState$: Observable<EditExerciseState>;
  error: string = null;

  constructor(private store: Store<AppState>, private route: ActivatedRoute) { }

  ngOnInit() {
    this.exerciseState$ = this.store.select(getEditExerciseState);
    const uuid = this.route.snapshot.params['uuid'];
    this.store.dispatch(new EditExerciseAction(uuid));
  }

}
