import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditExercisePageComponent } from './edit-exercise-page.component';

describe('EditExercisePageComponent', () => {
  let component: EditExercisePageComponent;
  let fixture: ComponentFixture<EditExercisePageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditExercisePageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditExercisePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
