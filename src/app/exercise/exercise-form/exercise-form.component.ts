import { Component, OnInit, Input } from '@angular/core';
import { MatTableDataSource } from '@angular/material';
import { Types } from '../../enums';
import { WorkoutComponent } from '../../shared/models/workout-component.model';
import { Exercise } from '../../shared/models/exercise.model';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators, FormBuilder, FormArray, ValidationErrors } from '@angular/forms';
import { isNull } from '@angular/compiler/src/output/output_ast';
import { getFormErrors } from '../../shared/helper.functions';
import { SaveExerciseAction } from '../../state/actions/exercises.actions';
import { AppState } from '../../state/app.state';
import { Store } from '@ngrx/store';

@Component({
  selector: 'app-exercise-form',
  templateUrl: './exercise-form.component.html',
  styleUrls: ['./exercise-form.component.scss']
})
export class ExerciseFormComponent implements OnInit {

  @Input()
  exercise: Exercise;

  workoutComponents = new MatTableDataSource();
  displayedColumns = [
    'name',
    'type',
    'unit',
    'delete'
  ];

  exerciseForm: FormGroup;

  constructor(private router: Router, private fb: FormBuilder, private store: Store<AppState>) { }

  get components(): FormArray {
    return <FormArray>this.exerciseForm.get('components');
  }

  ngOnInit() {
    this.exerciseForm = this.fb.group({
      name: new FormControl(this.exercise.Name, [Validators.required, Validators.minLength(3)]),
      components: this.fb.array([], [Validators.required])
    });
    this.onChanges();
    this.exercise.DefaultComponents.forEach(x => {
      const control = this.initRow();
      this.components.push(control);
      control.setValue({
        name: x.Name,
        type: x.Type,
        unit: x.UnitOfMeasurement
      });
    });
    this.workoutComponents.data = this.components.controls;
  }

  typesEnumToString(type: Types): string {
    return Types[type];
  }

  isTimespan(type: Types): boolean {
    return type === Types.Time;
  }

  delete(componentIndex: number) {
    this.components.removeAt(componentIndex);
    this.workoutComponents.data = this.components.controls;
  }

  cancel() {
    this.router.navigate(['/exercise/manage']);
  }

  save() {
    if (this.exerciseForm.valid) {
      const exercise: Exercise = {
        ExerciseUuid: this.exercise.ExerciseUuid,
        Name: this.exerciseForm.get('name').value,
        DefaultComponents: this.components.controls.map((x): WorkoutComponent => {
          return {
            Name: <string>x.get('name').value,
            Type: <Types>x.get('type').value,
            UnitOfMeasurement: <string>x.get('unit').value,
            Value: null
          };
        })
      };
      this.store.dispatch(new SaveExerciseAction(exercise));
    }
  }

  initRow(): FormGroup {
    return this.fb.group({
      name: new FormControl('', [Validators.required, Validators.minLength(3)]),
      type: new FormControl(0, [Validators.required]),
      unit: new FormControl('', [Validators.required])
    });
  }

  add() {
    this.components.push(this.initRow());
    this.workoutComponents.data = this.components.controls;
  }

  getFormValidationErrors() {
    return getFormErrors(this.exerciseForm);
  }

  onChanges() {
    this.components.valueChanges.subscribe((x: Array<any>) => {
      x.forEach((z, i) => {
        if (z.type === Types.Time && this.components.get(i.toString()).get('unit').enabled) {
          this.components.get(i.toString()).get('unit').disable();
        } else if (z.type !== Types.Time && !this.components.get(i.toString()).get('unit').enabled) {
          this.components.get(i.toString()).get('unit').enable();
        }
      });
    });
  }
}
