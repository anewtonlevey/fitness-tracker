import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListExercisesComponent } from './list-exercises/list-exercises.component';
import { ExerciseRoutingModule } from './exercise-routing.module';
import { SharedModule } from '../shared/shared.module';
import { ManageExercisesPageComponent } from './manage-exercises-page/manage-exercises-page.component';
import { CreateExercisePageComponent } from './create-exercise-page/create-exercise-page.component';
import { EditExercisePageComponent } from './edit-exercise-page/edit-exercise-page.component';
import { ExerciseFormComponent } from './exercise-form/exercise-form.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    ExerciseRoutingModule
  ],
  declarations: [ListExercisesComponent, ManageExercisesPageComponent, CreateExercisePageComponent, EditExercisePageComponent, ExerciseFormComponent]
})
export class ExerciseModule { }
