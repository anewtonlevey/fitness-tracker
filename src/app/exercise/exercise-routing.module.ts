import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListExercisesComponent } from './list-exercises/list-exercises.component';
import { ManageExercisesPageComponent } from './manage-exercises-page/manage-exercises-page.component';
import { CreateExercisePageComponent } from './create-exercise-page/create-exercise-page.component';
import { EditExercisePageComponent } from './edit-exercise-page/edit-exercise-page.component';

const routes: Routes = [{
    path: '',
    redirectTo: 'manage',
    pathMatch: 'full'
  }, {
    path: 'manage',
    component: ManageExercisesPageComponent
  }, {
    path: 'manage/create',
    component: CreateExercisePageComponent
  }, {
    path: 'manage/edit/:uuid',
    component: EditExercisePageComponent
  }];

  @NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
  })
  export class ExerciseRoutingModule { }
