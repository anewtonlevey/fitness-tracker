import { Component, OnInit } from '@angular/core';
import { ExercisesState } from '../../state/app.state';
import { Store } from '@ngrx/store';
import { getExerciseState } from '../../state/reducers/exercises.reducer';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-manage-exercises-page',
  templateUrl: './manage-exercises-page.component.html',
  styleUrls: ['./manage-exercises-page.component.scss']
})
export class ManageExercisesPageComponent implements OnInit {
  loading: boolean = true;
  iconName = 'add';
  constructor(private store: Store<ExercisesState>, private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    this.store.select(getExerciseState).subscribe(state => {
      this.loading = state.loading;
    });
  }

  createExercise() {
    this.router.navigate(['./create'], {
      relativeTo: this.route
    });
  }
}
