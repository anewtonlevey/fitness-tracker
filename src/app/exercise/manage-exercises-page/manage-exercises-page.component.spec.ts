import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageExercisesPageComponent } from './manage-exercises-page.component';

describe('ManageExercisesPageComponent', () => {
  let component: ManageExercisesPageComponent;
  let fixture: ComponentFixture<ManageExercisesPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageExercisesPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageExercisesPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
