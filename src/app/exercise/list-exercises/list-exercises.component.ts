import { Component, OnInit, ViewChild } from '@angular/core';
import { Store } from '@ngrx/store';
import { ExercisesState } from '../../state/app.state';
import { LoadExercisesAction } from '../../state/actions/exercises.actions';
import { Observable } from '@firebase/util';
import { Exercise } from '../../shared/models/exercise.model';
import { getExerciseState } from '../../state/reducers/exercises.reducer';
import { MatSort, MatTableDataSource, Sort } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-list-exercises',
  templateUrl: './list-exercises.component.html',
  styleUrls: ['./list-exercises.component.scss']
})
export class ListExercisesComponent implements OnInit {

  exercises = new MatTableDataSource();
  loading: Boolean = true;
  errorMessage: string = null;

  displayedColumns = [
    'name',
    'measurements',
    'edit'
  ];

  @ViewChild(MatSort) sort: MatSort;

  constructor(private store: Store<ExercisesState>, private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    this.store.dispatch(new LoadExercisesAction());
    this.store.select(getExerciseState).subscribe(state => {
      this.exercises.data = state.exercises;
      this.loading = state.loading;
      this.errorMessage = state.errorMessage;
    });
    this.exercises.sort = this.sort;
  }

  deleteExercise(uuid: string) {
    alert(uuid);
    console.log(uuid);
  }

  editExercise(uuid: string) {
    this.router.navigate(['./edit', uuid], {
      relativeTo: this.route
    });
  }

  sortData(sort: Sort) {
    const data = this.exercises.data.slice();
    if (!sort.active || sort.direction === '') {
      this.exercises.data = data;
      return;
    }

    this.exercises.data = data.sort((a: Exercise, b: Exercise) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'name': return compare(a.Name.toUpperCase(), b.Name.toUpperCase(), isAsc);
        case 'measurements': return compare(a.DefaultComponents.map(x => x.Name.toUpperCase()).reduce((acc, cv) => cv + ' '), b.DefaultComponents.map(x => x.Name.toUpperCase()).reduce((acc, cv) => cv + ' '), isAsc);
        default: return 0;
      }
    });
  }
}

function compare(a, b, isAsc) {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}
