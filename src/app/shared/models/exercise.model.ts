import { WorkoutComponent } from './workout-component.model';

export interface Exercise {
    ExerciseUuid: string;
    Name: string;
    DefaultComponents: Array<WorkoutComponent>;
}


export class ExerciseCls implements Exercise {
    ExerciseUuid: string =  null;
    Name: string = null;
    DefaultComponents: WorkoutComponent[] = [];
}
