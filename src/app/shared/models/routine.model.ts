import { WorkoutComponent } from './workout-component.model';

export interface Routine {
    ExerciseUuid: string;
    Components: Array<WorkoutComponent>;
}