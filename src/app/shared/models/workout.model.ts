import { Routine } from './routine.model';

export interface Workout {
    Datestamp: string;
    Routines: Array<Routine>;
}