interface GoogleInnerError {
    domain: string;
    message: string;
    reason: string;
}

export interface GoogleError {
    code: number;
    message: string;
    status: string;
    errors: Array<GoogleInnerError>;
}
