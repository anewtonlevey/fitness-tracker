import { Types } from '../../enums';

export interface WorkoutComponent {
    Name: string;
    Value: any;
    Type: Types;
    UnitOfMeasurement: string;
}
