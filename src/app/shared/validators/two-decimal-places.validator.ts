import { ValidatorFn, AbstractControl } from "@angular/forms";

function countDecimals(value: number): number {
    if (value !== null && Math.floor(value) !== value && value.toString().includes('.')) {
        return value.toString().split('.')[1].length || 0;
    }
    return 0;
}
export function TwoDecimalPlacesValidator(): ValidatorFn {
    return (control: AbstractControl): {[key: string]: any} | null => {
        const value = <number> control.value;
        const decimalPlaces = countDecimals(value);
        return decimalPlaces > 2 ? { 'twoDecimalPlaces': {value: control.value} } : null;
    };
}

