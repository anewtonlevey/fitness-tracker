import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NumberEasyInputComponent } from './number-easy-input.component';

describe('NumberEasyInputComponent', () => {
  let component: NumberEasyInputComponent;
  let fixture: ComponentFixture<NumberEasyInputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NumberEasyInputComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NumberEasyInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
