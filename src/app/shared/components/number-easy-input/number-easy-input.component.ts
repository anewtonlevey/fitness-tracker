import { Component, OnInit, Input, forwardRef } from '@angular/core';
import { FormControl, FormGroup, Validators, ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { TwoDecimalPlacesValidator } from '../../validators/two-decimal-places.validator';

export const EASY_NUMBER_VALUE_ACCESSOR = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => NumberEasyInputComponent),
  multi: true
};

@Component({
  selector: 'app-number-easy-input',
  templateUrl: './number-easy-input.component.html',
  styleUrls: ['./number-easy-input.component.scss'],
  providers: [EASY_NUMBER_VALUE_ACCESSOR]
})
export class NumberEasyInputComponent implements OnInit, ControlValueAccessor {

  @Input() placeholder: string;
  @Input() required: boolean = false;
  @Input() step: number = 1;
  @Input() disabled: boolean = false;
  @Input() defaultAfterNull: number = 0;

  easyNumberForm = new FormGroup({
    input: new FormControl('', [Validators.required, Validators.min(0), Validators.max(300), TwoDecimalPlacesValidator()])
  });

  constructor() {
  }


  onChange = (_: any) => {};
  onTouched = () => {};

  ngOnInit() {
    this.outputValue();
  }


  increment() {
    this.easyNumberForm.patchValue({
      input: (Math.round(this.getValue() / this.step) * this.step) + parseFloat(this.step.toString())
    });
    this.easyNumberForm.get('input').markAsDirty();
    this.easyNumberForm.get('input').markAsTouched();
    this.outputValue();
   }

  decrement() {
      this.easyNumberForm.patchValue({
        input: (Math.round(this.getValue() / this.step) * this.step) -  parseFloat(this.step.toString())
      });
      this.easyNumberForm.get('input').markAsDirty();
      this.easyNumberForm.get('input').markAsTouched();
      this.outputValue();
  }

  getValue() {
    let val = parseFloat(this.easyNumberForm.value.input);
    if (isNaN(val)) {
      val = this.defaultAfterNull;
    }
    return val;
  }

  outputValue() {
    this.pushChanges(this.getValue());
  }

  pushChanges(value: any) {
    this.onChange(value);
  }

  writeValue(value: any): void {
    this.easyNumberForm.patchValue({
      input:  value || null
    });

  }
  registerOnChange(fn: any): void {
    this.onChange = fn;
  }
  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }
  setDisabledState?(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

}
