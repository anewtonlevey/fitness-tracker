import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { LoginComponent } from './login/login.component';
import { HomePageComponent } from './home-page/home-page.component';
import { AuthGuard } from './services/auth.guard';
import { LogWeightComponent } from './log-weight/log-weight.component';

const routes: Routes = [{
  path: '',
  component: HomePageComponent
}, {
  path: 'login',
  component: HomePageComponent
}, {
  path: 'log-weight',
  component: LogWeightComponent,
  canActivate: [ AuthGuard ]
}, {
  path: 'exercise',
  loadChildren: '../exercise/exercise.module#ExerciseModule',  
  canActivate: [ AuthGuard ]
}, {
  path: '**',
  component: PageNotFoundComponent
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class CoreRoutingModule { }
