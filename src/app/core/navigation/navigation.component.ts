import { Component, OnInit,  ViewChild } from '@angular/core';
import { BreakpointObserver, Breakpoints, BreakpointState } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { NavState, AuthenticationState } from '../../state/app.state';
import * as navigationReducer from '../../state/reducers/navigation.reducer';
import * as authenticationReducer from '../../state/reducers/authentication.reducer';
import { MatSidenav, MatButton } from '@angular/material';
import { OpenSideNavAction, CloseSideNavAction, ToggleSideNavAction } from '../../state/actions/navigation.actions';
import { take } from 'rxjs/operators';
import { UnAuthenticateAction } from '../../state/actions/authentication.actions';
import { User } from '../../shared/models/user.model';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {

  @ViewChild('drawer') drawer: MatSidenav;
  @ViewChild('openButton') openButton: MatButton;

  isHandset: boolean = false;
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );
  authState$: Observable<AuthenticationState>;
  user$: Observable<User>;

  constructor(private breakpointObserver: BreakpointObserver, private store: Store<NavState>) {

  }

  public CloseSideNav(isSwipe: boolean) {
    if (!isSwipe || this.isHandset) {
      this.store.dispatch(new CloseSideNavAction());
    }
  }

  public OpenSideNav(isSwipe: boolean) {
    if (!isSwipe || this.isHandset) {
      this.store.dispatch(new OpenSideNavAction());
    }
  }
  public ToggleSideNav(isSwipe: boolean) {
    this.store.dispatch(new ToggleSideNavAction());
  }

  public Logout() {
    this.store.dispatch(new UnAuthenticateAction());
  }

  ngOnInit(): void {
    this.isHandset$.subscribe(x => {
      this.isHandset = x;
      if (x) {
        this.CloseSideNav(false);
      } else {
        this.OpenSideNav(false);
      }
    });
    this.store.select(navigationReducer.getSideNavOpen).subscribe(x => {
        this.drawer.toggle(x);
        if (this.openButton && !x) { this.openButton._getHostElement().blur(); }
    });
    this.authState$ = this.store.select(authenticationReducer.getAuthState);
    this.user$ = this.store.select(authenticationReducer.getUser);
  }

  }
