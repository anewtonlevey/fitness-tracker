import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LogWeightComponent } from './log-weight.component';

describe('LogWeightComponent', () => {
  let component: LogWeightComponent;
  let fixture: ComponentFixture<LogWeightComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LogWeightComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LogWeightComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
