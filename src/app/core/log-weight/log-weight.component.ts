import { Component, OnInit } from '@angular/core';
import { Validators, FormControl, FormGroup } from '@angular/forms';
import * as moment from 'moment';
import { TwoDecimalPlacesValidator } from '../../shared/validators/two-decimal-places.validator';
import { Weight } from '../../shared/models/weight.model';
import { VALID } from '../../constants';
import { WeightState } from '../../state/app.state';
import { Store } from '@ngrx/store';
import { SaveWeightAction } from '../../state/actions/weight.actions';
import * as weightReducer from '../../state/reducers/weight.reducer';
import { Observable } from 'rxjs';
import { getUser } from '../../state/reducers/authentication.reducer';
import { User } from '../../shared/models/user.model';

@Component({
  selector: 'app-log-weight',
  templateUrl: './log-weight.component.html',
  styleUrls: ['./log-weight.component.scss']
})
export class LogWeightComponent implements OnInit {
  saving$: Observable<boolean>;

  logWeightForm = new FormGroup({
    weight: new FormControl('', [Validators.required, Validators.min(0), Validators.max(300), TwoDecimalPlacesValidator()])
  });



  constructor(private store: Store<WeightState>) { }

  ngOnInit() {
    this.saving$ = this.store.select(weightReducer.getWeightSaving);
    this.store.select(weightReducer.getWeight).subscribe(x => {
      let weight: number = null;
      if (x === 'Unknown') {
        weight = null;
      } else {
        weight = x;
      }

      this.logWeightForm.patchValue({
        weight: weight
      });
    });
  }

  logWeight() {
    if (this.logWeightForm.status === VALID) {
      const weight: Weight = this.logWeightForm.value;
      weight.timestamp = moment().valueOf();
      this.store.select(getUser).subscribe(u =>
        this.store.dispatch(new SaveWeightAction(weight, u.uid)));
    }
  }
}
