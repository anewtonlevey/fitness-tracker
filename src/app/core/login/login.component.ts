import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AuthenticationState } from '../../state/app.state';
import { RequestGoogleAuthAction, AuthSucceededAction, AuthFailedAction } from '../../state/actions/authentication.actions';
import { AngularFireAuth } from 'angularfire2/auth';
import { User } from '../../shared/models/user.model';
import { Router } from '@angular/router';
import * as authenticationReducer from '../../state/reducers/authentication.reducer';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private store: Store<AuthenticationState>,
  private router: Router) { }

  googleAuth() {
    this.store.dispatch(new RequestGoogleAuthAction());
  }

  ngOnInit() {
    this.store.select(authenticationReducer.getAuthState).subscribe(authState => {
      if (authState.isAuthenticated) {
        this.router.navigate(['']);
      }
    });

    this.store.select(authenticationReducer.getError).pipe(filter(x => x !== null)).subscribe(err => {
      console.error(err);
    });
  }

}
