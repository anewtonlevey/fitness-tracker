import { Component, OnInit } from '@angular/core';
import { AuthenticationState } from '../../state/app.state';
import { Store } from '@ngrx/store';
import { User } from '../../shared/models/user.model';
import * as authenticationReducer from '../../state/reducers/authentication.reducer';
import * as weightReducer from '../../state/reducers/weight.reducer';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-action-menu',
  templateUrl: './action-menu.component.html',
  styleUrls: ['./action-menu.component.scss']
})
export class ActionMenuComponent implements OnInit {
  user$: Observable<User>;
  weight$: Observable<number | 'Unknown'>;
  weight: string = 'Unknown';

  constructor(private store: Store<AuthenticationState>) { }

  ngOnInit() {
    this.user$ = this.store.select(authenticationReducer.getUser);
    this.weight$ = this.store.select(weightReducer.getWeight);

    this.weight$.subscribe(x => {
      if (x !== 'Unknown' ) {
        this.weight = x + 'kg';
      }  else {
        this.weight = x;
      }
    });
  }

}
