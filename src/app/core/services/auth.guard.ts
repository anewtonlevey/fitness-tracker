import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import * as authenticationReducer from '../../state/reducers/authentication.reducer';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { AuthenticationState } from '../../state/app.state';
import { map, take, tap, filter } from 'rxjs/operators';
import { AngularFireAuth } from 'angularfire2/auth';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private store: Store<AuthenticationState>, private router: Router, private afAuth: AngularFireAuth) {}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
      return this.afAuth.authState.pipe(map(authState => {
        return !!authState;
      }));
  }
}
