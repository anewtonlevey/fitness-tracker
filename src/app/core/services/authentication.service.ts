import { Injectable } from '@angular/core';
import { ReplaySubject, Observable, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { User } from '../../shared/models/user.model';
import { AngularFireAuth } from 'angularfire2/auth';
import { AuthenticationState } from '../../state/app.state';
import { Store } from '@ngrx/store';
import { AUTHENTICATION_SUCCEEDED, AuthSucceededAction, NotAuthenticatedAction } from '../../state/actions/authentication.actions';


@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor(private afAuth: AngularFireAuth,
  private store: Store<AuthenticationState>) {
    this.afAuth.authState.pipe(
      switchMap((user) => {
          return of(user);
      })
    ).subscribe(fbUser => {
      if (fbUser) {
        const user: User  = {
          uid: fbUser.uid,
          name: fbUser.displayName,
          email: fbUser.email,
          roles: {}
        };
        this.store.dispatch(new AuthSucceededAction(user));
      } else {
        this.store.dispatch(new NotAuthenticatedAction());
      }
    });
  }
}
