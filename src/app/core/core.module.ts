import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppComponent } from './app.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { NavigationComponent } from './navigation/navigation.component';
import { SharedModule } from '../shared/shared.module';
import { CoreRoutingModule } from './core-routing.module';
import { LoginComponent } from './login/login.component';
import { HomePageComponent } from './home-page/home-page.component';
import { LoginOrSignupComponent } from './login-or-signup/login-or-signup.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AuthenticationService } from './services/authentication.service';
import { ActionMenuComponent } from './action-menu/action-menu.component';
import { LogWeightComponent } from './log-weight/log-weight.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    PageNotFoundComponent,
    NavigationComponent,
    LoginComponent,
    HomePageComponent,
    LoginOrSignupComponent,
    ActionMenuComponent,
    LogWeightComponent,
  ],
imports: [
    CommonModule,
    SharedModule,
    CoreRoutingModule,
    BrowserAnimationsModule,
    ReactiveFormsModule
  ],
  exports: [
    AppComponent
  ],
  providers: [AuthenticationService]
})
export class CoreModule { }
