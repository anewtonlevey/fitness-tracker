import { Component, OnInit } from '@angular/core';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';
import { Router, NavigationStart } from '@angular/router';
import { filter, map } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { AppState } from '../state/app.state';
import { CloseSideNavAction } from '../state/actions/navigation.actions';
import { Observable } from 'rxjs';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { AuthenticationService } from './services/authentication.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  isHandset: boolean = false;
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );

  constructor(private breakpointObserver: BreakpointObserver,
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer,
    private router: Router,
    private store: Store<AppState>,
    private authService: AuthenticationService) {

    matIconRegistry.addSvgIcon('facebook', domSanitizer.bypassSecurityTrustResourceUrl('../assets/icons/facebook.svg'));
    matIconRegistry.addSvgIcon('google', domSanitizer.bypassSecurityTrustResourceUrl('../assets/icons/google.svg'));
    router.events.pipe(filter((event) => event instanceof NavigationStart)).subscribe(e => {
      if (this.isHandset) {
        store.dispatch(new CloseSideNavAction());
      }
    });
    this.isHandset$.subscribe(x => {
      this.isHandset = x;
    });
  }

  ngOnInit() {
  }
}
