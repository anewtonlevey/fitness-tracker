import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { trigger, transition, style, animate } from '@angular/animations';
import * as authenticationReducer from '../../state/reducers/authentication.reducer';
import { AuthenticationState } from '../../state/app.state';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss'],
  animations: [
    trigger('fadeInOut', [
      transition(':enter', [   // :enter is alias to 'void => *'
        style({opacity: 0}),
        animate(600, style({opacity: 1}))
      ]),
      transition(':leave', [   // :leave is alias to '* => void'
        animate(600, style({opacity: 0}))
      ])
    ])
  ]
})
export class HomePageComponent implements OnInit {
  authState$: Observable<AuthenticationState>;

  constructor(private router: Router,
    private store: Store<AuthenticationState>) { }

  ngOnInit() {
    this.authState$ = this.store.select(authenticationReducer.getAuthState);
  }

}
