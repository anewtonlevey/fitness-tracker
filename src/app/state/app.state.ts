import { User } from '../shared/models/user.model';
import { Observable } from 'rxjs';
import { Exercise } from '../shared/models/exercise.model';

export interface AppState {
    navigationState: NavState;
    authenticationState: AuthenticationState;
    logWeightState: WeightState;
    exerciseState: ExercisesState;
}

export const NavState = 'navigationState';
export interface NavState {
    sideNavOpen: boolean;
}

export const AuthenticationState = 'authenticationState';
export interface AuthenticationState {
    isAuthenticated: boolean; // TODO: Remove this, as this is actually (user && authResolved)
    user: User;
    errorMessage: string;
}

export const WeightState = 'logWeightState';
export interface WeightState {
    saving: boolean;
    lastWeight: number | 'Unknown';
}


export const ExercisesState = 'exerciseState';
export interface ExercisesState {
    loading: boolean;
    exercises: Array<Exercise>;
    errorMessage: string;
    editingExercise: EditExerciseState;
}

export interface EditExerciseState {
    exercise: Exercise;
    saving: boolean;
    errorMessage: string;
    loading: boolean;
    exerciseToEditUuid: string;
}
