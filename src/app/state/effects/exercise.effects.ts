import { Injectable } from '@angular/core';
import { Action, Store } from '@ngrx/store';
import { Router } from '@angular/router';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable, of, from, forkJoin, merge } from 'rxjs';
import { ExercisesState, AppState } from '../app.state';
import { AngularFirestore } from 'angularfire2/firestore';
import { switchMap, catchError, tap, mergeMap, filter, map, withLatestFrom } from 'rxjs/operators';
import { AngularFireAuth } from 'angularfire2/auth';
import { User } from '../../shared/models/user.model';
import { filterQueryId } from '@angular/core/src/view/util';
import { LOAD_EXERCISES, LoadExercisesAction, LoadExercisesFailedAction, LoadExercisesSuccededAction, EDIT_EXERCISE, LOAD_EXERCISES_SUCEEDED, EditExerciseAction, SaveExerciseAction, SAVE_EXERCISE, SaveExerciseSucceededAction, SaveExerciseFailedAction } from '../actions/exercises.actions';
import { USER_COLLECTION, EXERCISE_SUB_COLLECTION } from '../../constants';
import { Exercise } from '../../shared/models/exercise.model';
import { uuid } from '../../shared/helper.functions';


@Injectable()
export class ExerciseEffects {

    constructor(
        private actions: Actions,
        private router: Router,
        private db: AngularFirestore,
        private store: Store<AppState>
    ) { }

    @Effect({ dispatch: true })
    LoadExercises: Observable<Action> = this.actions.pipe(
        ofType(LOAD_EXERCISES),
        withLatestFrom(this.store),
        switchMap(([action, state]) => {
            const userExercisesCollection = this.db.collection(USER_COLLECTION).doc(state.authenticationState.user.uid).collection(EXERCISE_SUB_COLLECTION).ref;
            return from(userExercisesCollection.get()).pipe(
                switchMap(ref => {
                    const docs = ref.docs.map(docRef => <Exercise>docRef.data());
                    return of(new LoadExercisesSuccededAction(docs.sort((a, b) => {
                        const A = a.Name.toUpperCase();
                        const B = b.Name.toUpperCase();
                        return (A < B) ? -1 : (A > B) ? 1 : 0;
                    })));
                }),
                catchError((err: Error) => {
                    return of(new LoadExercisesFailedAction(err.message));
                })
            );
        })
    );

    @Effect({ dispatch: false })
    CheckExercisesLoadedOnEdit: Observable<Action> = this.actions.pipe(
        ofType(EDIT_EXERCISE),
        withLatestFrom(this.store),
        switchMap(([action, state]) => {
            if (!state.exerciseState.loading && state.exerciseState.exercises.length < 1) {
                this.store.dispatch(new LoadExercisesAction());
            }
            return of(null);
        })
    );

    @Effect({dispatch: false})
    AfterExercisesLoadStartEditing: Observable<Action> = this.actions.pipe(
        ofType(LOAD_EXERCISES_SUCEEDED),
        withLatestFrom(this.store),
        switchMap(([action, state]) => {
            if (state.exerciseState.editingExercise.loading) {
                this.store.dispatch(new EditExerciseAction(state.exerciseState.editingExercise.exerciseToEditUuid));
            }
            return of(null);
        })
    );

    @Effect()
    SaveExercise: Observable<Action> = this.actions.pipe(
        ofType(SAVE_EXERCISE),
        withLatestFrom(this.store),
        switchMap(([action, state]: [SaveExerciseAction, AppState]) => {
            const userExercisesCollection = this.db.collection(USER_COLLECTION).doc(state.authenticationState.user.uid).collection(EXERCISE_SUB_COLLECTION).ref;
            if (action.exercise.ExerciseUuid === null) {
                // Create Uuid
                action.exercise.ExerciseUuid = uuid();
            }
            return from(userExercisesCollection.doc(action.exercise.ExerciseUuid).set(action.exercise)).pipe(
                map(() => new SaveExerciseSucceededAction(action.exercise)),
                catchError((err: Error) => of(new SaveExerciseFailedAction(err.message)))
            );
        }),
        tap(() => this.router.navigate(['exercise/manage']))
    );
}
