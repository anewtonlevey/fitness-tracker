import { Injectable } from '@angular/core';
import { Action, Store } from '@ngrx/store';
import { Router } from '@angular/router';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable, of, from, forkJoin } from 'rxjs';
import { WeightState } from '../app.state';
import { AngularFirestore } from 'angularfire2/firestore';
import { LOAD_LAST_WEIGHT, LoadLastWeightAction, LoadLastWeightSuceededAction, LoadLastWeightFailedAction, SAVE_WEIGHT, SaveWeightAction, SaveWeightSuceededAction, SaveWeightFailedAction, SAVE_WEIGHT_FAILED, SAVE_WEIGHT_SUCEEDED } from '../actions/weight.actions';
import { WEIGHT_COLLECTION, WEIGHT_RECORDED_SUBCOLLECTION } from '../../constants';
import { switchMap, catchError, tap, mergeMap, filter, map } from 'rxjs/operators';
import { Weight } from '../../shared/models/weight.model';
import { AngularFireAuth } from 'angularfire2/auth';
import { User } from '../../shared/models/user.model';
import { filterQueryId } from '@angular/core/src/view/util';


@Injectable()
export class WeightEffects {

    constructor(
        private actions: Actions,
        private router: Router,
        private db: AngularFirestore,
        private store: Store<WeightState>
    ) { }

    @Effect({ dispatch: true })
    LoadWeight: Observable<Action> = this.actions.pipe(
        ofType(LOAD_LAST_WEIGHT),
        switchMap((action: LoadLastWeightAction) => {
            return from(this.db.collection(WEIGHT_COLLECTION).doc(action.uid).ref.get()).pipe(
                tap(ref => {
                    if (!ref.exists) {
                        throw new Error('Document does not exist');
                    }
                }),
                map(ref => <Weight>ref.data()),
                switchMap(weight => {
                return of(new LoadLastWeightSuceededAction(weight));
            }),
            catchError((err: Error) => {
                return of(new LoadLastWeightFailedAction(err.message));
            })
        );
        })
    );

    @Effect({ dispatch: true })
    SaveWeight: Observable<Action> = this.actions.pipe(
        ofType(SAVE_WEIGHT),
        switchMap((action: SaveWeightAction) => {
            return forkJoin([
                from(this.db.collection(WEIGHT_COLLECTION).doc(action.uid).collection(WEIGHT_RECORDED_SUBCOLLECTION).add(action.weight)),
                from(this.db.collection(WEIGHT_COLLECTION).doc(action.uid).set(action.weight, { merge: true }))
            ]).pipe(
                switchMap(x => {
                    return of(new SaveWeightSuceededAction(action.weight));
                }),
                catchError((err: Error) => {
                    console.error(err.message);
                    return of(new SaveWeightFailedAction());
                })
            );
        })
    );

    @Effect({ dispatch: false })
    WeightSaved: Observable<Action> = this.actions.pipe(
        ofType(SAVE_WEIGHT_SUCEEDED, SAVE_WEIGHT_FAILED),
        tap(action => {
            this.router.navigate(['/']);
        })
    );
}
