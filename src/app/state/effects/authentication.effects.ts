import { Injectable } from '@angular/core';
import { Action, Store } from '@ngrx/store';
import { Router } from '@angular/router';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable, of, from } from 'rxjs';
import { REQUEST_GOOGLE_AUTHENTICATION, RequestGoogleAuthAction, AuthSucceededAction, AuthFailedAction, UNAUTHENTICATE, UnAuthenticateAction, AUTHENTICATION_SUCCEEDED } from '../actions/authentication.actions';
import { mergeMap, tap, map, switchMap, catchError } from 'rxjs/operators';
import { AngularFireAuth } from 'angularfire2/auth';
import { auth } from 'firebase';
import { GoogleError } from '../../shared/models/google-error.model';
import { AuthenticationState } from '../app.state';
import { AngularFirestore } from 'angularfire2/firestore';
import { USER_COLLECTION } from '../../constants';
import { LoadLastWeightAction } from '../actions/weight.actions';
import * as weightReducer from '../../state/reducers/weight.reducer';

@Injectable()
export class AuthenticationEffects {

  constructor(
    private actions: Actions,
    private router: Router,
    private afAuth: AngularFireAuth,
    private db: AngularFirestore,
    private store: Store<AuthenticationState>
  ) {}

  // effects go here
@Effect({dispatch: true})
  GoogleAuthentication: Observable<Action> = this.actions.pipe(
    ofType(REQUEST_GOOGLE_AUTHENTICATION),
    switchMap((action: RequestGoogleAuthAction) => {
      return from(this.afAuth.auth.signInWithRedirect(new auth.GoogleAuthProvider())).pipe(
        tap(),
        catchError((err: Error) => {
          return of(new AuthFailedAction(err.message));
        })
      );
    })
  );

  @Effect({dispatch: false})
  Unauthenticate: Observable<Action> = this.actions.pipe(
    ofType(UNAUTHENTICATE),
    tap((action: UnAuthenticateAction) => {
      this.afAuth.auth.signOut()
      .then(() => {
          this.router.navigate(['/']);
      })
      .catch(err => {
        console.error(err);
      });
    })
  );

  @Effect({dispatch: false})
  AuthenticationSuceededUpdateData: Observable<Action> = this.actions.pipe(
    ofType(AUTHENTICATION_SUCCEEDED),
    tap((action: AuthSucceededAction) => {
      this.db.collection(USER_COLLECTION).doc(action.user.uid).set(action.user, { merge: true });
    })
  );

  @Effect({dispatch: false})
  AuthenticationSuceededLoadData: Observable<Action> = this.actions.pipe(
    ofType(AUTHENTICATION_SUCCEEDED),
    tap((action: AuthSucceededAction) => {
      this.store.dispatch(new LoadLastWeightAction(action.user.uid));
    })
  );
}
