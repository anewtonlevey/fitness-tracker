import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { Weight } from '../../shared/models/weight.model';

/**
 * Actions
 */

 export const LOAD_LAST_WEIGHT_SUCEEDED = 'LOAD_LAST_WEIGHT_SUCEEDED';
 export const LOAD_LAST_WEIGHT_FAILED = 'LOAD_LAST_WEIGHT_FAILED';
 export const LOAD_LAST_WEIGHT = 'LOAD_LAST_WEIGHT';
 export const SAVE_WEIGHT = 'SAVE_WEIGHT';
 export const SAVE_WEIGHT_FAILED = 'SAVE_WEIGHT_FAILED';
 export const SAVE_WEIGHT_SUCEEDED = 'SAVE_WEIGHT_SUCEEDED';



 /**
  * Action Types
  */
export class SaveWeightAction implements Action {
    readonly type = SAVE_WEIGHT;
    readonly weight: Weight;
    readonly uid: string;
    constructor(weight: Weight, uid: string) {
        this.weight = weight;
        this.uid = uid;
    }
}

export class SaveWeightFailedAction implements Action {
    readonly type = SAVE_WEIGHT_FAILED;
}


export class SaveWeightSuceededAction implements Action {
    readonly type = SAVE_WEIGHT_SUCEEDED;
    readonly weight: Weight;
    constructor(weight: Weight) {
        this.weight = weight;
    }
}


export class LoadLastWeightAction implements Action {
    readonly type = LOAD_LAST_WEIGHT;
    readonly uid: string;
    constructor(uid: string) {
        this.uid = uid;
    }
}

export class LoadLastWeightFailedAction implements Action {
    readonly type = LOAD_LAST_WEIGHT_FAILED;
    readonly error: string;
    constructor(error: string) {
        this.error = error;
    }
}

export class LoadLastWeightSuceededAction implements Action {
    readonly type = LOAD_LAST_WEIGHT_SUCEEDED;
    readonly weight: Weight;
    constructor(weight: Weight) {
        this.weight = weight;
    }
}

export type WeightActions =
SaveWeightAction |
SaveWeightFailedAction |
SaveWeightSuceededAction |
LoadLastWeightAction |
LoadLastWeightFailedAction |
LoadLastWeightSuceededAction
;
