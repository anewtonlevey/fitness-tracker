import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { Exercise } from '../../shared/models/exercise.model';

/**
 * Actions
 */

 export const LOAD_EXERCISES = 'LOAD_EXERCISES';
 export const LOAD_EXERCISES_FAILED = 'LOAD_EXERCISES_FAILED';
 export const LOAD_EXERCISES_SUCEEDED = 'LOAD_EXERCISES_SUCCEEDED';



 /**
  * Action Types
  */


export class LoadExercisesAction implements Action {
    readonly type = LOAD_EXERCISES;
}

export class LoadExercisesFailedAction implements Action {
    readonly type = LOAD_EXERCISES_FAILED;
    readonly error: string;
    constructor(error: string) {
        this.error = error;
    }
}

export class LoadExercisesSuccededAction implements Action {
    readonly type = LOAD_EXERCISES_SUCEEDED;
    readonly exercises: Array<Exercise>;
    constructor(exercises: Array<Exercise>) {
        this.exercises = exercises;
    }
}

export const EDIT_EXERCISE = 'EDIT_EXERCISE';
export const SAVE_EXERCISE = 'SAVE_EXERCISE';
export const SAVE_EXERCISE_FAILED = 'SAVE_EXERCISE_FAILED';
export const SAVE_EXERCISE_SUCCEEDED = 'SAVE_EXERCISE_SUCCEEDED';

export class EditExerciseAction implements Action {
    readonly type = EDIT_EXERCISE;
    readonly uuid: string;
    constructor(uuid: string) {
        this.uuid = uuid;
    }
}

export class SaveExerciseAction implements Action {
    readonly type = SAVE_EXERCISE;
    readonly exercise: Exercise;
    constructor(exercise: Exercise) {
        this.exercise = exercise;
    }
}

export class SaveExerciseFailedAction implements Action {
    readonly type = SAVE_EXERCISE_FAILED;
    readonly error: string;
    constructor(error: string) {
        this.error = error;
    }
}

export class SaveExerciseSucceededAction implements Action {
    readonly type = SAVE_EXERCISE_SUCCEEDED;
    readonly exercise: Exercise;
    constructor(exercise: Exercise) {
        this.exercise = exercise;
    }
}


export type ExerciseActions =
LoadExercisesAction |
LoadExercisesFailedAction |
LoadExercisesSuccededAction |
EditExerciseAction |
SaveExerciseAction |
SaveExerciseFailedAction |
SaveExerciseSucceededAction
;
