import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';

/**
 * Actions
 */

 export const OPEN_SIDE_NAV = 'OPEN_SIDE_NAV';
 export const CLOSE_SIDE_NAV = 'CLOSE_SIDE_NAV';
 export const TOGGLE_SIDE_NAV = 'TOGGLE_SIDE_NAV';



 /**
  * Action Types
  */
export class OpenSideNavAction implements Action {
    readonly type = OPEN_SIDE_NAV;
}

export class CloseSideNavAction implements Action {
    readonly type = CLOSE_SIDE_NAV;
}

export class ToggleSideNavAction implements Action {
    readonly type = TOGGLE_SIDE_NAV;
}

export type NavActions =
OpenSideNavAction |
CloseSideNavAction |
ToggleSideNavAction
;
