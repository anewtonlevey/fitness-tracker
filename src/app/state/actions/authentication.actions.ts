import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { User } from '../../shared/models/user.model';

/**
 * Actions
 */

export const REQUEST_GOOGLE_AUTHENTICATION = 'REQUEST_GOOGLE_AUTHENTICATION';
export const AUTHENTICATION_FAILED = 'AUTHENTICATION_FAILED';
export const AUTHENTICATION_SUCCEEDED = 'AUTHENTICATION_SUCCEEDED';
export const UNAUTHENTICATE = 'UNAUTHENTICATE';
export const NOT_AUTHENTICATED = 'NOT_AUTHENTICATED';



/**
 * Action Types
 */
export class RequestGoogleAuthAction implements Action {
   readonly type = REQUEST_GOOGLE_AUTHENTICATION;
}

export class AuthFailedAction implements Action {
   readonly type = AUTHENTICATION_FAILED;
   readonly message: string;
   constructor(message: string) {
       this.message = message;
   }
}

export class AuthSucceededAction implements Action {
   readonly type = AUTHENTICATION_SUCCEEDED;
   readonly user: User;
   constructor(user: User) {
       this.user = user;
   }
}

export class NotAuthenticatedAction implements Action {
    readonly type = NOT_AUTHENTICATED;
}

export class UnAuthenticateAction implements Action {
    readonly type = UNAUTHENTICATE;
}



export type AuthActions =
RequestGoogleAuthAction |
AuthFailedAction |
AuthSucceededAction |
NotAuthenticatedAction |
UnAuthenticateAction
;

