import { createFeatureSelector, createSelector } from '@ngrx/store';
import { ExercisesState } from '../app.state';
import { ExerciseActions, LOAD_EXERCISES, LOAD_EXERCISES_SUCEEDED, LOAD_EXERCISES_FAILED, EDIT_EXERCISE, SAVE_EXERCISE, SAVE_EXERCISE_FAILED, SAVE_EXERCISE_SUCCEEDED } from '../actions/exercises.actions';

export const initialState: ExercisesState = {
    exercises: [],
    loading: false,
    errorMessage: null,
    editingExercise: {
        errorMessage: null,
        exercise: null,
        saving: false,
        loading: false,
        exerciseToEditUuid: null
    }
};

export function exerciseReducer(state = initialState, action: ExerciseActions): ExercisesState {
    switch (action.type) {
        case LOAD_EXERCISES:
            return {
                ...state,
                loading: true,
                exercises: [],
                errorMessage: null
            };
        case LOAD_EXERCISES_SUCEEDED:
            return {
                ...state,
                loading: false,
                exercises: action.exercises,
                errorMessage: null
            };
        case LOAD_EXERCISES_FAILED:
            return {
                ...state,
                loading: false,
                exercises: [],
                errorMessage: action.error,
                editingExercise: {
                    ...state.editingExercise,
                    loading: false,
                    errorMessage: 'There was an error attempting to edit this exercise.',
                    exercise: null
                }
            };
        case EDIT_EXERCISE:
        const exerciseToEdit = state.exercises.find(x => x.ExerciseUuid === action.uuid);
            return {
                ...state,
                editingExercise: {
                    ...state.editingExercise,
                    errorMessage: null,
                    exercise: (exerciseToEdit) ? exerciseToEdit : null,
                    saving: false,
                    loading:  (exerciseToEdit) ? false : true,
                    exerciseToEditUuid: action.uuid
                }
            };
        case SAVE_EXERCISE:
            return {
                ...state,
                editingExercise: {
                    ...state.editingExercise,
                    errorMessage: null,
                    exercise: action.exercise,
                    saving: true
                }
            };
        case SAVE_EXERCISE_FAILED:
            return {
                ...state,
                editingExercise: {
                    ...state.editingExercise,
                    errorMessage: action.error,
                    exercise: null,
                    saving: false
                }
            };
        case SAVE_EXERCISE_SUCCEEDED:
            const isAdd = !state.exercises.some(e => e.ExerciseUuid === action.exercise.ExerciseUuid);
            let exercises = state.exercises;
            if (isAdd) {
                exercises.push(action.exercise);
            } else {
                exercises = exercises
                    .filter(e => e.ExerciseUuid === action.exercise.ExerciseUuid);
                exercises.push(action.exercise);
            }
            return {
                ...state,
                exercises: exercises,
                editingExercise: {
                    ...state.editingExercise,
                    errorMessage: null,
                    exercise: null,
                    saving: false
                }
            };
        default:
            return state;
    }
}

/**
 * Selectors
 */
export const getExerciseFeatureState = createFeatureSelector<ExercisesState>(ExercisesState);

export const getExerciseState = createSelector(
    getExerciseFeatureState,
    (state: ExercisesState) => state
);

export const getEditExerciseState = createSelector(
    getExerciseFeatureState,
    (state: ExercisesState) => state.editingExercise
);
