import { createFeatureSelector, createSelector } from '@ngrx/store';
import { WeightState } from '../app.state';
import { WeightActions, LOAD_LAST_WEIGHT, LOAD_LAST_WEIGHT_FAILED, LOAD_LAST_WEIGHT_SUCEEDED, SAVE_WEIGHT, SAVE_WEIGHT_FAILED, SAVE_WEIGHT_SUCEEDED } from '../actions/weight.actions';

export const initialState: WeightState = { lastWeight: 'Unknown', saving: false };

export function reducer(state = initialState, action: WeightActions): WeightState {
    switch (action.type) {
        case LOAD_LAST_WEIGHT_FAILED:
            return {
                ...state,
                lastWeight: 'Unknown'
            };
        case LOAD_LAST_WEIGHT_SUCEEDED:
            return {
                ...state,
                lastWeight: action.weight.weight
            };
        case SAVE_WEIGHT:
            return {
                ...state,
                saving: true
            };
        case SAVE_WEIGHT_SUCEEDED:
            return {
                ...state,
                saving: false,
                lastWeight: action.weight.weight
            };
        case SAVE_WEIGHT_FAILED:
            return {
                ...state,
                saving: false
            };
        default:
            return state;
    }
}

/**
 * Selectors
 */
export const getWeightFeatureState = createFeatureSelector<WeightState>(WeightState);

export const getWeight = createSelector(
    getWeightFeatureState,
    (state: WeightState) => state.lastWeight
);

export const getWeightSaving = createSelector(
    getWeightFeatureState,
    (state: WeightState) => state.saving
);

