import { ActionReducerMap, ActionReducer, MetaReducer } from '@ngrx/store';
import { AppState } from '../app.state';
import { environment } from '../../../environments/environment';
import * as NavigationReducer from './navigation.reducer';
import * as AuthenticationReducer from './authentication.reducer';
import * as WeightReducer from './weight.reducer';
import { exerciseReducer } from './exercises.reducer';

export const reducers: ActionReducerMap<AppState> = {
    navigationState: NavigationReducer.reducer,
    authenticationState: AuthenticationReducer.reducer,
    logWeightState: WeightReducer.reducer,
    exerciseState: exerciseReducer
};

export function logger(reducer: ActionReducer<AppState>): ActionReducer<AppState> {
    return function (state: AppState, action: any): AppState {
        console.log('state: ', state);
        console.log('action: ', action);
        return reducer(state, action);
    };
}


export const metaReducers: MetaReducer<AppState>[] = !environment.production
    ? [logger]
    : [];
