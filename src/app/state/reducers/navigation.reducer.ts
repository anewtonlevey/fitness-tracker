import { createFeatureSelector, createSelector } from '@ngrx/store';
import { NavActions, OPEN_SIDE_NAV, CLOSE_SIDE_NAV, TOGGLE_SIDE_NAV } from '../actions/navigation.actions';
import { NavState } from '../app.state';

export const initialState: NavState = { sideNavOpen: false };

export function reducer(state = initialState, action: NavActions): NavState {
    switch (action.type) {
        case OPEN_SIDE_NAV: {
            return {
                ...state,
                sideNavOpen: true
            };
        }
        case CLOSE_SIDE_NAV: {
            return {
                ...state,
                sideNavOpen: false
            };
        }
        case TOGGLE_SIDE_NAV: {
            return {
                ...state,
                sideNavOpen: !state.sideNavOpen
            };
        }
        default: {
            return state;
        }
    }
}

/**
 * Selectors
 */
export const getNavFeatureState = createFeatureSelector<NavState>(NavState);

export const getSideNavOpen = createSelector(
    getNavFeatureState,
    (state: NavState) => state.sideNavOpen
);

export const getNavState = createSelector(
    getNavFeatureState,
    (state: NavState) => state
);

