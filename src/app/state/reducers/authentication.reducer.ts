import { createFeatureSelector, createSelector } from '@ngrx/store';
import { AuthActions, AUTHENTICATION_SUCCEEDED, AUTHENTICATION_FAILED, REQUEST_GOOGLE_AUTHENTICATION, NOT_AUTHENTICATED, UNAUTHENTICATE } from '../actions/authentication.actions';
import { AuthenticationState } from '../app.state';

export const initialState: AuthenticationState = { isAuthenticated: false, user: null, errorMessage: null };

export function reducer(state = initialState, action: AuthActions): AuthenticationState {
    switch (action.type) {
        case AUTHENTICATION_SUCCEEDED: {
            return {
                ...state,
                isAuthenticated: true,
                user: action.user,
                errorMessage: null
            };
        }
        case REQUEST_GOOGLE_AUTHENTICATION:
        case NOT_AUTHENTICATED:
        case UNAUTHENTICATE: {
            return {
                ...state,
                isAuthenticated: false,
                user: null,
                errorMessage: null
            };
        }
        case AUTHENTICATION_FAILED: {
            return {
                ...state,
                isAuthenticated: false,
                user: null,
                errorMessage: action.message
            };
        }
        default: {
            return state;
        }
    }
}

/**
 * Selectors
 */
export const getAuthFeatureState = createFeatureSelector<AuthenticationState>(AuthenticationState);

export const getAuthState = createSelector(
    getAuthFeatureState,
    (state: AuthenticationState) => state
);

export const getUser = createSelector(
    getAuthFeatureState,
    (state: AuthenticationState) => state.user
);

export const getError = createSelector(
    getAuthFeatureState,
    (state: AuthenticationState) => state.errorMessage
);

