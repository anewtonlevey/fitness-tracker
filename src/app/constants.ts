export const USER_COLLECTION = 'users';

export const WEIGHT_COLLECTION = 'weight';
export const WEIGHT_RECORDED_SUBCOLLECTION = 'recorded';

export const EXERCISE_SUB_COLLECTION = 'exercise';



export const VALID = 'VALID';
export const INVALID = 'INVALID';
